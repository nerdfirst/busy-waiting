#define LED_BLINK 2
#define LED_PRESS 3
#define BTN 7

void setup() {
  pinMode(LED_BLINK, OUTPUT);
  pinMode(LED_PRESS, OUTPUT);
  pinMode(BTN, INPUT);
}

void loop() {
  digitalWrite(LED_BLINK, LOW);
  busyWait(500);
  digitalWrite(LED_BLINK, HIGH);
  busyWait(500);
}

void busyWait(long duration) {
  long end_time = millis() + duration;
  while (millis() < end_time) {
    if (digitalRead(BTN) == HIGH) {
      digitalWrite(LED_PRESS, HIGH);
    }
    else {
      digitalWrite(LED_PRESS, LOW);
    }
  }
}

